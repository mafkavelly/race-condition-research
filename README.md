# Race Condition Research
Researching best ways to fix race conditions with Threads and Coroutines

## Usage
Run CoroutineDataTest or ThreadDataTest

## Authors and acknowledgment
The main idea is taken from
https://kotlinlang.org/docs/shared-mutable-state-and-concurrency.html
