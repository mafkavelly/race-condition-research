interface SharedMutableData {
    var mutableState: Int
    fun run()
}